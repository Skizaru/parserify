package com.example.parserify.tools.providers;

import android.content.Context;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

public class MetroLyricsProvider extends LyricsProvider {

    public MetroLyricsProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        super(musicMetaDataViewModel, context);

    }

    @Override
    public void execute() {
        MusicMetaData data = musicMetaDataViewModel.getMusicMetaData().getValue();
        String reworkedArtist = LyricsToolbox.removeAccents(data.getArtist().replace("\'", "").replace(" ", "-").toLowerCase());
        String reworkedTrack = LyricsToolbox.removeAccents(data.getTitle().replace("\'", "").replace(" ", "-").toLowerCase());
        String url ="http://www.metrolyrics.com/" + reworkedTrack + "-lyrics-" + reworkedArtist + ".html";
        this.fetchLyrics(url, "metrolyrics.net");
    }
}
