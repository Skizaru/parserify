package com.example.parserify.tools.providers;

import android.content.Context;

import com.example.parserify.model.MusicMetaDataViewModel;

public class ProvidersManager {

    private Context context;
    private MusicMetaDataViewModel musicMetaDataViewModel;

    public ProvidersManager(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        this.context = context;
        this.musicMetaDataViewModel = musicMetaDataViewModel;
    }

    public void fetchLyrics() {
            ParolesNetProvider parolesNetProvider = new ParolesNetProvider(musicMetaDataViewModel, context);
            MetroLyricsProvider metroLyricsProvider = new MetroLyricsProvider(musicMetaDataViewModel, context);
            Paroles2ChansonProvider paroles2ChansonProvider = new Paroles2ChansonProvider(musicMetaDataViewModel, context);

            parolesNetProvider.setNext(metroLyricsProvider);
            metroLyricsProvider.setNext(paroles2ChansonProvider);

            parolesNetProvider.execute();
    }

    public void fetchArtistInfos() {
        LastFmInfosProvider lastFmInfosProvider = new LastFmInfosProvider(musicMetaDataViewModel, context);

        lastFmInfosProvider.execute();
    }
}
