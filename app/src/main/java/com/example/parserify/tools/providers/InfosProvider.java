package com.example.parserify.tools.providers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class InfosProvider {

    private Context context;

    protected MusicMetaDataViewModel musicMetaDataViewModel;
    protected InfosProvider next;

    public InfosProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        this.context = context;
        this.musicMetaDataViewModel = musicMetaDataViewModel;
    }

    public void fetchInfos(String url, String provider) {
        RequestQueue queue = Volley.newRequestQueue(this.context);
        Map<String, String> artistInfos = new HashMap<>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    String infos;
                    String imgUrl;
                    infos = InfosToolbox.parseInfosDom(response, provider);
                    imgUrl = InfosToolbox.getArtistImageUrl(response, provider);
                    if (infos != null) {
                        artistInfos.put("ORIGINAL", infos);
                        if (imgUrl != null) {
                            musicMetaDataViewModel.setInfosImg(imgUrl);
                        }
                        musicMetaDataViewModel.setArtistInfos(artistInfos);
                    } else {
                        if (next != null)
                            next.execute();
                    }
                }, error -> {
            if (next != null)
                next.execute();
        });
        queue.add(stringRequest);
    }

    public abstract void execute();
}
