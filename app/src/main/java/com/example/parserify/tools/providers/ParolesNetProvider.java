package com.example.parserify.tools.providers;

import android.content.Context;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

public class ParolesNetProvider extends LyricsProvider {

    public ParolesNetProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        super(musicMetaDataViewModel, context);

    }

    @Override
    public void execute() {
        MusicMetaData data = musicMetaDataViewModel.getMusicMetaData().getValue();
        String reworkedArtist = LyricsToolbox.removeAccents(data.getArtist().replace('\'', '-').replace(' ', '-'));
        String reworkedTrack = LyricsToolbox.removeAccents(data.getTitle().replace('\'', '-').replace('’', '-').replace(' ', '-'));
        String url ="https://www.paroles.net/" + reworkedArtist + "/paroles-" + reworkedTrack;
        this.fetchLyrics(url, "paroles.net");
    }
}
