package com.example.parserify.tools.providers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.Map;

public class InfosToolbox {
    private static Map<Character, Character> MAP_NORM;
    public static String parseInfosDom(String lyricsDom, String provider) {
        final Document doc = Jsoup.parse(lyricsDom);
        String parsedLyrics = "";
        Elements elements;
        switch (provider) {
            case "last.fm":
                elements = doc.select(".metadata-and-wiki-row--artist");
                parsedLyrics += "Born the " + elements.select(".catalogue-metadata-description").text() + "\n\n";
                parsedLyrics += elements.select(".wiki-column").text() + "\n";
                elements = doc.select(".tags-list--global");
                String url = doc.select("link[rel=canonical]").attr("href") + "/+wiki";
                parsedLyrics += "<a href=\"" + url + "\">" + url + "</a>";
                if (elements != null && !elements.isEmpty()) {
                    parsedLyrics += "\n\n" + "Tags :\n";
                    for(Element child : elements.select(".tag")) {
                        parsedLyrics += (child).text() + "\n";
                    }
                }
                break;
            default:
                parsedLyrics = null;
        }
        if (parsedLyrics != null && !parsedLyrics.isEmpty()) {
            parsedLyrics = Jsoup.clean(parsedLyrics, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
        }
        return parsedLyrics;
    }

    public static String getArtistImageUrl(String LyricsDom, String provider) {
        final Document doc = Jsoup.parse(LyricsDom);
        String imgUrl = null;
        switch (provider) {
            case "last.fm":
                // Image class in page header-new-background-image
                imgUrl = doc.select(".header-new-background-image").attr("style");
                imgUrl = imgUrl.replaceAll("background\\-image:\\surl\\(", "").replaceAll("\\)\\;", "");
                break;
            default:
                imgUrl = null;
        }
        if (imgUrl == null || imgUrl.isEmpty()) {
            imgUrl = null;
        }
        return imgUrl;
    }
}
