package com.example.parserify.tools.providers;

import android.content.Context;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

import java.util.Locale;

public class LastFmInfosProvider extends InfosProvider {

    public LastFmInfosProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        super(musicMetaDataViewModel, context);
    }

    @Override
    public void execute() {
        MusicMetaData data = musicMetaDataViewModel.getMusicMetaData().getValue();
        String url = LastFmInfosProvider.getUrl(data.getArtist());
        this.fetchInfos(url, "last.fm");
    }

    public static String getUrl(String artist) {
        return "https://www.last.fm/music/" + artist.replace(" ", "+");
    }
}
