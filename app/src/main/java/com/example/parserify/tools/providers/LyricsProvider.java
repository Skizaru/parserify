package com.example.parserify.tools.providers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class LyricsProvider {

    private Context context;

    protected MusicMetaDataViewModel musicMetaDataViewModel;
    private LyricsProvider next;

    private List<String> languages = Arrays.asList("fr", "en", "es", "ja");

    LyricsProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        this.context = context;
        this.musicMetaDataViewModel = musicMetaDataViewModel;
    }

    void fetchLyrics(String url, String provider) {
        RequestQueue queue = Volley.newRequestQueue(this.context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    String lyrics;
                    lyrics = LyricsToolbox.parseLyricsDom(response, provider);
                    if (lyrics.length() > 0) {
                        Map<String, String> lyricsStore = new HashMap<>();
                        lyricsStore.put("ORIGINAL", lyrics);
                        musicMetaDataViewModel.setLyrics(lyricsStore);
                        fetchTranslation();
                    } else {
                        if (next != null)
                            next.execute();
                    }
                }, error -> {
            if (next != null)
                next.execute();
        });
        queue.add(stringRequest);
    }

    private void fetchTranslation() {
        Map<String, String> lyricsStore = musicMetaDataViewModel.getLyrics().getValue();

        RequestQueue queue = Volley.newRequestQueue(this.context);
        String encodedLyrics;

        try {
            encodedLyrics = URLEncoder.encode(lyricsStore.get("ORIGINAL"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            encodedLyrics = lyricsStore.get("ORIGINAL");
        }

        for (String lang : languages) {
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, String.format("https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=%s&dt=t&q=%s", lang, encodedLyrics), null,
                    response -> {
                        Map<String, String> newLyrics = new HashMap<>();
                        newLyrics.putAll(musicMetaDataViewModel.getLyrics().getValue());
                        try {
                            JSONArray json = response.getJSONArray(0);
                            String lyrics = "";
                            for (int i = 0; i < json.length(); i++) {
                                lyrics += json.getJSONArray(i).getString(0);
                            }
                            newLyrics.put(lang, lyrics);
                            musicMetaDataViewModel.setLyrics(newLyrics);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("User-Agent", "Chromium");
                    return params;
                }
            };
            queue.add(stringRequest);
        }
    }

    void setNext(LyricsProvider next) {
        this.next = next;
    }

    public abstract void execute();
}
