package com.example.parserify.tools.providers;

import android.content.Context;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

public class Paroles2ChansonProvider extends LyricsProvider {

    public Paroles2ChansonProvider(MusicMetaDataViewModel musicMetaDataViewModel, Context context) {
        super(musicMetaDataViewModel, context);

    }

    @Override
    public void execute() {
        MusicMetaData data = musicMetaDataViewModel.getMusicMetaData().getValue();
        String reworkedArtist = LyricsToolbox.removeAccents(data.getArtist().replace("\'", "-").replace(" ", "-").toLowerCase());
        String reworkedTrack = LyricsToolbox.removeAccents(data.getTitle().replace("\'", "-").replace(" ", "-").toLowerCase());
        String url ="https://paroles2chansons.lemonde.fr/paroles-" + reworkedArtist + "/paroles-" + reworkedTrack + ".html";
        this.fetchLyrics(url, "paroles2chansons.lemonde.fr");
    }
}
