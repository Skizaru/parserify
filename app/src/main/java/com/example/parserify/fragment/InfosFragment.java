package com.example.parserify.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.parserify.R;
import com.example.parserify.model.MusicMetaDataViewModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class InfosFragment extends Fragment {

    private MusicMetaDataViewModel musicMetaDataViewModel;

    public InfosFragment() {
        // Required empty public constructor
    }

    public static InfosFragment newInstance() {
        InfosFragment fragment = new InfosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_infos, container, false);

        musicMetaDataViewModel = ViewModelProviders.of(this.getActivity()).get(MusicMetaDataViewModel.class);
        musicMetaDataViewModel.getArtistInfos().observe(this, artistInfos -> {
            this.updateUi();
        });

        return view;
    }

    protected void updateUi(){
        TextView infosTextView = getView().findViewById(R.id.infosTextView);
        infosTextView.setText(musicMetaDataViewModel.getArtistInfos().getValue().get("ORIGINAL"));
        new DownloadImageTask(getView().findViewById(R.id.infos_fragment_imageView)).execute(musicMetaDataViewModel.getInfosImg().getValue());
        Linkify.addLinks(infosTextView, Linkify.ALL);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


}