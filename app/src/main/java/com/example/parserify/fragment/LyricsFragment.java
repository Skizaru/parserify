package com.example.parserify.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.parserify.R;
import com.example.parserify.model.MusicMetaDataViewModel;


public class LyricsFragment extends Fragment {

    private MusicMetaDataViewModel musicMetaDataViewModel;

    public LyricsFragment() {
        // Required empty public constructor
    }

    public static LyricsFragment newInstance() {
        LyricsFragment fragment = new LyricsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lyrics, container, false);
        musicMetaDataViewModel = ViewModelProviders.of(this.getActivity()).get(MusicMetaDataViewModel.class);

        musicMetaDataViewModel.getLyrics().observe(this, lyrics -> {
            this.updateUi();
        });

        // Inflate the layout for this fragment
        return view;
    }

    protected void updateUi(){
        TextView lyricsTextView = getView().findViewById(R.id.lyricsTextView);
        lyricsTextView.setText(musicMetaDataViewModel.getLyrics().getValue().get("ORIGINAL"));
    }


}
