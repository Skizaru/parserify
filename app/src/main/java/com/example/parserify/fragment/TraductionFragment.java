package com.example.parserify.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.parserify.R;
import com.example.parserify.model.MusicMetaDataViewModel;

import java.util.Arrays;
import java.util.List;


public class TraductionFragment extends Fragment {

    private MusicMetaDataViewModel musicMetaDataViewModel;

    private List<String> languages = Arrays.asList("fr", "en", "es", "ja");
    private String selectedLanguage = "fr";

    public TraductionFragment() {
        // Required empty public constructor
    }

    public static TraductionFragment newInstance() {
        return new TraductionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        musicMetaDataViewModel = ViewModelProviders.of(getActivity()).get(MusicMetaDataViewModel.class);
        musicMetaDataViewModel.getLyrics().observe(this, lyrics -> this.updateUi());

        View view = inflater.inflate(R.layout.fragment_traduction, container, false);

        Spinner dropDownLanguages = view.findViewById(R.id.dropDownLanguages);
        dropDownLanguages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(musicMetaDataViewModel.getLyrics().getValue() != null && musicMetaDataViewModel.getLyrics().getValue().get(languages.get(position)) != null){
                    TextView lyricsTextView = view.findViewById(R.id.traductionTextView);
                    lyricsTextView.setText(musicMetaDataViewModel.getLyrics().getValue().get(languages.get(position)));
                }
                selectedLanguage = languages.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                TextView lyricsTextView = view.findViewById(R.id.traductionTextView);
                lyricsTextView.setText(musicMetaDataViewModel.getLyrics().getValue().get("ORIGINAL"));
            }
        });

        return view;
    }

    private void updateUi(){
        TextView lyricsTextView = getView().findViewById(R.id.traductionTextView);
        lyricsTextView.setText(musicMetaDataViewModel.getLyrics().getValue().get(selectedLanguage));
    }

}