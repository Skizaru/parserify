package com.example.parserify;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;
import com.example.parserify.receiver.SpotifyIntentReceiver;
import com.example.parserify.tools.providers.ProvidersManager;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    /* Potential providers ::
    https://www.nautiljon.com/paroles/mongol800/chiisana+koi+no+uta.html
    https://genius.com/Bob-marley-and-the-wailers-no-woman-no-cry-lyrics       (Class :: lyrics)
     */

    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;

    private SpotifyIntentReceiver spotifyIntentReceiver;
    private MusicMetaDataViewModel musicMetaDataViewModel;

    ProvidersManager urlBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.mainTabLayout);
        viewPager = findViewById(R.id.fragment_viewPager);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        musicMetaDataViewModel = ViewModelProviders.of(this).get(MusicMetaDataViewModel.class);
        urlBuilder = new ProvidersManager(musicMetaDataViewModel, this);

        musicMetaDataViewModel.getMusicMetaData().observe(this, musicMetaData -> {
                this.updateUi();
                this.urlBuilder.fetchLyrics();
                this.urlBuilder.fetchArtistInfos();
        });

        // TabLayout Manager
        PagerAdapter adapter = new PagerAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager;
        tabLayout.setupWithViewPager(viewPager);

        IntentFilter filter = new IntentFilter("com.spotify.music.metadatachanged");
        spotifyIntentReceiver = new SpotifyIntentReceiver(musicMetaDataViewModel);
        this.registerReceiver(spotifyIntentReceiver, filter);

        SearchView searchView = findViewById(R.id.searchField);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("spotify:search:" + searchView.getQuery().toString())));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setQuery("", false);
        searchView.clearFocus();

    }

    protected void updateUi(){
        MusicMetaData data = musicMetaDataViewModel.getMusicMetaData().getValue();
        TextView songName = this.findViewById(R.id.songName);
        TextView artistName = this.findViewById(R.id.artistName);
        songName.setText(data.getTitle());
        artistName.setText(data.getArtist().toUpperCase());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SearchView searchView = findViewById(R.id.searchField);
        searchView.clearFocus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(spotifyIntentReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.openSpotify) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("spotify:")));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
