package com.example.parserify.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MusicMetaData{

    private String title;
    private String artist;
    private String album;

    public MusicMetaData(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}
