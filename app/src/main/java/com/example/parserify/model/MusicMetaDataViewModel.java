package com.example.parserify.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Map;

public class MusicMetaDataViewModel extends ViewModel {

    private MutableLiveData<MusicMetaData> musicMetaData;

    private MutableLiveData<Map<String, String>> lyrics;

    private MutableLiveData<Map<String, String>> artistInfos;
    private MutableLiveData<String> infosImg;

    public MusicMetaDataViewModel() {
        musicMetaData = new MutableLiveData<>();
        lyrics = new MutableLiveData<>();
        artistInfos = new MutableLiveData<>();
        infosImg = new MutableLiveData<>();
    }

    public LiveData<MusicMetaData> getMusicMetaData() {
        return musicMetaData;
    }

    public void setMusicMetaData(MusicMetaData musicMetaData) {
        this.musicMetaData.setValue(musicMetaData);
    }

    public LiveData<Map<String, String>> getLyrics() {
        return lyrics;
    }

    public void setLyrics(Map<String, String> lyrics) {
        this.lyrics.setValue(lyrics);
    }

    public LiveData<Map<String, String>> getArtistInfos() {
        return artistInfos;
    }

    public void setArtistInfos(Map<String, String> artistInfos) {
        this.artistInfos.setValue(artistInfos);
    }

    public LiveData<String> getInfosImg() {
        return infosImg;
    }

    public void setInfosImg(String infosImg) {
        this.infosImg.setValue(infosImg);
    }
}
