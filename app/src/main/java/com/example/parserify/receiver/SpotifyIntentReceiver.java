package com.example.parserify.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.parserify.model.MusicMetaData;
import com.example.parserify.model.MusicMetaDataViewModel;

public class SpotifyIntentReceiver extends BroadcastReceiver {

    private MusicMetaDataViewModel viewModel;

    public SpotifyIntentReceiver(MusicMetaDataViewModel viewModel){
        this.viewModel = viewModel;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String artist = intent.getStringExtra("artist");
        String album = intent.getStringExtra("album");
        String track = intent.getStringExtra("track");

        MusicMetaData data = new MusicMetaData();
        data.setArtist(artist);
        data.setAlbum(album);
        data.setTitle(track);

        viewModel.setMusicMetaData(data);

    }

}
