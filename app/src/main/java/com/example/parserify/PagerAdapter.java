package com.example.parserify;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.parserify.fragment.InfosFragment;
import com.example.parserify.fragment.LyricsFragment;
import com.example.parserify.fragment.TraductionFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context mContext;

    public PagerAdapter(Context context, FragmentManager fm, int NumOfTabs) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mNumOfTabs = NumOfTabs;
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment tab1 = LyricsFragment.newInstance();
                return tab1;
            case 1:
                Fragment tab2 = TraductionFragment.newInstance();
                return tab2;
            case 2:
                Fragment tab3 = InfosFragment.newInstance();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.lyricsTabName);
            case 1:
                return mContext.getString(R.string.traductionTabName);
            case 2:
                return mContext.getString(R.string.infosTabName);
            default:
                return "default";
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
